// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#pragma once

#ifndef SIMULATION_H
#define SIMULATION_H

#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <memory>

#include <Eigen/Sparse>
#include <Eigen/Dense>

#include "util.h"
#include "constants.h"
#include "particle.h"
#include "communicator.h"
#include "output_file.h"
#include "ionize_particles.h"

//Solver control constants
static const int GMRES_RESTART = 9;
static const int GMRES_MAX_ITERS = 8;
static const double GMRES_TOLERANCE = 1e-10;
static const int SCHWARZ_ITERS = 8;

static const int NEWTON_MAX_ITERS = 8;
static const double NEWTON_TOLERANCE = 1e-8;
static const double DERIVATION_DELTA = 1e-7;

//Main simulation class
class Simulation {
	private:
		const std::unordered_map<std::string, double> parameters; //key = value hash table holding config file information

		bool debug = true;

		Eigen::Vector3i cpus_xyz; //how many ranks (CPUs) along each axis
		Eigen::Vector3i global_size; //size of the global simulation (in cells)
		Eigen::Vector3i ghost_cells; //how many ghost cells in each direction

		Communicator comm; //our MPI communicator Object

		int Nx, Ny, Nz; //size of local simulation, xyz
		int Nt; //number of timesteps to perform

		Eigen::Vector3i inner_size; //local simulation size - ghost_cells
		Eigen::Vector3i total_size; //local simulation size + ghost_cells

		const double THETA;

		int timestep; //current timestep
		double time; //current time

		double dx; //cell size
		double cell_vol; //cell volume = dx^3
		double dt; //duration of single timestep
		double macro_e; //macro factor for electrons
		double macro_P; //macro factor for protons
		double macro; //global macro factor, currently set to macro_e
		double mp_me; //mass of proton / mass of electron
		double temperature; //temperature in eV
		int electronnumberbg;
		int protonnumberbg;
		int positronnumberbg;
		int electronnumberjet;
		int protonnumberjet;
		int positronnumberjet;

		int smooth_charge; //should charge density be smoothed in each step

		//sparse matrix operators
		Operator divergence;
		Operator gradient;
		Operator vector_laplace;
		Operator scalar_laplace;
		Operator identity;
		Operator curl_E;
		Operator curl_B;

		Operator nu_tensor_op;
		Operator tensor_divergence;

		//Sources and fields on the grid
		Field charge_density;
		Field current_density;
		Field pressure;
		Field dielectric;

		Field E;
		Field E_old;
		Field B;
		Field B_old;

		Field potential; //unused
		Field div_P;

		//Cross-function storage for Newton-Krylov solver
		Particle p_n;
		Eigen::Vector3d u_outer;
		Eigen::Vector3d F0;

		std::vector<Particle> parts; //List of particles

		std::ofstream out; //Energy output file

		Binary_output_files hdf_out; //HDF output file

		Ionize_particles ionization;

		double iteration_histogram[NEWTON_MAX_ITERS]; //how often did NK converge after N iterations
		double error_count; //how often did something go wrong during iterations (not necessarily fatal)
	public:
		Simulation(int argc, char* argv[]);
		void main_loop();

	private:
		std::unordered_map<std::string, double> load_parameters() const;

		void calc_sources();
		void calc_fields();
		void move_particles();
		void energy_output();

		// My additions
		void boundaries_processing();
		bool cell_in_a_circle(int x_global, int y_global, int z_global);

		void step();

		void stencil_scalar_laplace();
		void stencil_divergence();
		void stencil_gradient();
		void stencil_identity();

		void stencil_curl_E();
		void stencil_curl_B();

		void stencil_nu_tensor_op();
		void stencil_tensor_divergence();

		void smooth(Field& rho, int dim);

		//indexing helper routines, taking ghost cells into account, scalar, vector, tensor versions
		inline int sindg(int x, int y, int z) const {
			return(ind(x + ghost_cells[0], y + ghost_cells[1], z + ghost_cells[2], 0, total_size[0], total_size[1], total_size[2], 1));
		}
		inline int vindg(int x, int y, int z, int c) const {
			return(ind(x + ghost_cells[0], y + ghost_cells[1], z + ghost_cells[2], c, total_size[0], total_size[1], total_size[2], 3));
		}
		inline int tindg(int x, int y, int z, int c) const {
			return(ind(x + ghost_cells[0], y + ghost_cells[1], z + ghost_cells[2], c, total_size[0], total_size[1], total_size[2], 9));
		}
		inline int dindg(int x, int y, int z, int c, int Nc) const {
			return(ind(x + ghost_cells[0], y + ghost_cells[1], z + ghost_cells[2], c, total_size[0], total_size[1], total_size[2], Nc));
		}

		//inverse scalar indexing routine, no ghost cells taken into account
		inline void resind(int r, int& rx, int& ry, int& rz) const {
			rz = r%total_size[2]; r/=total_size[2]; ry = r%total_size[1]; r/=total_size[1]; rx = r%total_size[0];
		}

		//inverse scalar indexing routine, ghost cells taken into account
		inline void resindg(int r, int& rx, int& ry, int& rz) const {
			resind(r, rx, ry, rz);
			rx -= ghost_cells[0]; ry -= ghost_cells[1]; rz -= ghost_cells[2];
		}

		void init_sources();
		void init_parameters();
		void init_particles();
		void init_fields();
	
		void get_local_fields(const Field& electric, const Field& magnetic, const Eigen::Vector3d& pos, Eigen::Vector3d& E_loc, Eigen::Vector3d& B_loc, uint8_t flav) const;

		Eigen::Vector3d compute_r(const Eigen::Vector3d& u, const Eigen::Vector3d& dist, uint8_t flav) const;

		typedef Eigen::Matrix<double, GMRES_MAX_ITERS+1, GMRES_MAX_ITERS> RestartMatrix;
		typedef Eigen::Matrix<double, GMRES_MAX_ITERS+1, 3> Restart3DMatrix;
		typedef Eigen::Matrix<double, GMRES_MAX_ITERS+1, 1> RestartVector;
		Eigen::Vector3d nk_solve(Particle& p);
		int nk_iterate(Eigen::Vector3d& x, int& max_iter, double& tol, uint8_t flav);
		void nk_generate_plane_rotation(const double& dx, const double& dy, double& cs, double& sn) const;
		void nk_update(Eigen::Vector3d& x, int k, const RestartMatrix& h, const RestartVector& s, const Restart3DMatrix& v) const;
		void nk_apply_plane_rotation(double& dx, double& dy, const double& cs, const double& sn) const;
};

#endif
