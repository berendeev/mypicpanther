#ifndef OUTPUT_FILE
#define OUTPUT_FILE

#include <tuple>
#include <utility>
#include <vector>
#include <string>

#include "communicator.h" // it works only with a single mpi-process
#include "particle.h"


class Output_file {
protected:
    Communicator& comm;
    const double& dx;
    const double& dt;
	const int& timestep;
    std::vector<std::tuple<const std::string, const int, const Field* const, const int>> list_of_components;
    std::pair<int, const std::vector<Particle>*> output_particles;

    void create_skeleton();

public:
    Output_file(Communicator& comm, const double& dx, const double& dt, const double& time, const int& timestep)
    :   comm(comm), dx(dx), dt(dt), timestep(timestep) {};
    virtual ~Output_file() = default;

    void add_component_output(const std::string& name, int diagnostic_timestep, const Field& field, int component)
    {
    	if(diagnostic_timestep > 0)
    		list_of_components.push_back(make_tuple(name, diagnostic_timestep, &field, component));
    }

    void add_scalar_output(const std::string& name, int diagnostic_timestep, const Field& field) {};

    void add_particle_output(int diagnostic_timestep, const std::vector<Particle>& parts)
    {
    	if(diagnostic_timestep > 0)
    		output_particles = make_pair(diagnostic_timestep, &parts);
    }
    
    virtual void output() = 0;
    virtual void write_vector_field(const std::string& path, const Field& field, int component) = 0;
	virtual void particle_output(const std::vector<Particle>& parts) = 0;
};


// вроде не так сложно и с mpi сделать вывод
class Binary_output_files : public Output_file {
public:
    Binary_output_files(Communicator& comm, const double& dx, const double& dt, const double& time, const int& timestep);
    ~Binary_output_files() = default;

	void output() override;
	void write_vector_field(const std::string& path, const Field& field, int component) override;
	void particle_output(const std::vector<Particle>& parts) override;
};

#endif // OUTPUT_FILE