// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#include "particle.h"
#include "constants.h"

using Eigen::Matrix3d;
using Eigen::Vector3d;

using namespace std;

Properties Particle::p[AMOUNT_OF_SORTS] = {
	{0., 0., 0., 0.},
	{0., 0., 0., 0.},
	{0., 0., 0., 0.},
	{0., 0., 0., 0.}};

// constructor builds the particle from position in units of dx, velocity (v in units of c) and an ID
Particle::Particle(const Vector3d &position, const Vector3d &velocity, Tag tag) : r(position),
																				  u(velocity * SI::c / sqrt(1. - velocity.dot(velocity))),
																				  id(tag)
{
}

// calculate alpha tensor
Matrix3d Particle::get_alpha(const Vector3d &E_old, const Vector3d &B) const
{
	const double beta = get_beta();

	const double gp = beta / (SI::c * SI::c) * E_old.dot(get_v()) + get_gamma();
	const double betap = beta / gp;
	const double Dp = gp * (1. + betap * betap * B.dot(B));

	Matrix3d alpha;
	alpha << 1. + betap * betap * B[0] * B[0], betap * B[2] + betap * betap * B[0] * B[1], -betap * B[1] + betap * betap * B[0] * B[2],
		-betap * B[2] + betap * betap * B[0] * B[1], 1. + betap * betap * B[1] * B[1], betap * B[0] + betap * betap * B[1] * B[2],
		betap * B[1] + betap * betap * B[0] * B[2], -betap * B[0] + betap * betap * B[1] * B[2], 1. + betap * betap * B[2] * B[2];
	alpha *= 1. / Dp;

	return (alpha);
}

// calculate particle (velocity) moments: charge density, pressure, current density
void Particle::get_moments(double theta, const Vector3d &E, const Vector3d &B, Vector3d &j, Matrix3d &P, Matrix3d &nu) const
{
	const double &q = p[id.flav].q;
	const double &m = p[id.flav].m;
	const double &d = p[id.flav].density;
	const Matrix3d alpha = get_alpha(E, B);

	j = alpha * u;
	P << q * d * j[0] * j[0], q * d * j[0] * j[1], q * d * j[0] * j[2],
		 q * d * j[0] * j[1], q * d * j[1] * j[1], q * d * j[1] * j[2],
		 q * d * j[0] * j[2], q * d * j[1] * j[2], q * d * j[2] * j[2];
	j *= q * d;

	nu = alpha * (-(q * q * theta * dt * dt) / (2. * SI::eps0 * m) * d);
}

// move particle with given velocity
void Particle::move(const Vector3d &u_new)
{
	const double gamma = get_gamma();
	const double gamma_new = sqrt(1. + u_new.dot(u_new) / (SI::c * SI::c));
	r += dt / dx * ((u + u_new) / (gamma + gamma_new));

	u = u_new;
}

double Particle::dt = 0.;
double Particle::dx = 0.;
