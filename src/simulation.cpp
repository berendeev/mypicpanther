// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#include <algorithm>

#include "simulation.h"

using namespace std;

using Eigen::Vector3i;

//main loop just steps through each timestep and outputs Newton diagnostics at the end
void Simulation::main_loop() {
	for(int t = 0; t < Nt; t++) {
		step();
	}

	double tmp[NEWTON_MAX_ITERS];
	comm.global_sum(iteration_histogram[0], tmp[0], NEWTON_MAX_ITERS);
	if(comm.rank() == 0 && debug) {
		for(int i = 0; i < NEWTON_MAX_ITERS; i++)
			cout << i << ": " << tmp[i] << endl;
	}
}

//one complete simulation step
void Simulation::step() {
	if(comm.rank() == 0 && debug) 
		cout << "T: " << timestep << endl;
	
	// make preparation step
	if (!ionization.should_be_removed(timestep))
	{
		if(comm.rank() == 0 && debug)
			cout << "Ionization" << endl;

		ionization.execute(timestep);
	}

	if(comm.rank() == 0 && debug)
		cout << "Sources" << endl;

	//collect source terms
	calc_sources();

	if(comm.rank() == 0 && debug)
		cout << "Fields" << endl;

	//calculate advanced E/B-Fields
	calc_fields();

	if(comm.rank() == 0 && debug)
		cout << "Particles" << endl;

	//move all computational particles
	move_particles();

	//get E_n+1 from E_n+\Theta through interpolation
	E = (E - (1.-THETA) * E_old)/THETA;

	//set up proper boundary conditions
	if(comm.rank() == 0 && debug)
		cout << "Boundaries" << endl;

	boundaries_processing();

	if(comm.rank() == 0 && debug)
		cout << "Output" << endl;

	//output data
	hdf_out.output();
	energy_output();

	//collect diagnostics
	double etmp;
	comm.global_sum(error_count, etmp);
	if(comm.rank() == 0 && debug) {
		cout << "EE: " << etmp << endl;
	}

	//increment step counters
	timestep++;
	time += dt;
}

//constructor inits basics stuff
Simulation::Simulation(int argc, char* argv[]) :
	parameters(load_parameters()),

	cpus_xyz(
		parameters.at("procs_x"),
		parameters.at("procs_y"),
		parameters.at("procs_z")
	),
	global_size(
		parameters.at("N_x"),
		parameters.at("N_y"),
		parameters.at("N_z")
	),
	ghost_cells( //enough for TSC particles
		global_size[0] > 1 ? 4 : 0,
		global_size[1] > 1 ? 4 : 0,
		global_size[2] > 1 ? 4 : 0
	),
	comm(argc, argv, global_size, ghost_cells, cpus_xyz), //MPI

	Nx(comm.get_local_size()[0]),
	Ny(comm.get_local_size()[1]),
	Nz(comm.get_local_size()[2]),
	Nt(parameters.at("total_steps")),

	inner_size(Nx, Ny, Nz),
	total_size(inner_size + 2 * ghost_cells),

	THETA(parameters.at("theta")),

	timestep(0),
	time(0.),
	dx(0.),
	cell_vol(0.),
	dt(0.),
	macro_e(0.),
	macro_P(0.),
	macro(0.),
	mp_me(0.),
	temperature(0.),
	electronnumberbg(0),
	protonnumberbg(0),
	positronnumberbg(0),
	electronnumberjet(0),
	protonnumberjet(0),
	positronnumberjet(0),

	smooth_charge(parameters.at("smooth_charge")),

	divergence        (total_size.prod()  , total_size.prod()*3),
	gradient          (total_size.prod()*3, total_size.prod()  ),
	vector_laplace    (total_size.prod()*3, total_size.prod()*3),
	scalar_laplace    (total_size.prod()  , total_size.prod()  ),
	identity          (total_size.prod()*3, total_size.prod()*3),

	curl_E            (total_size.prod()*3, total_size.prod()*3),
	curl_B            (total_size.prod()*3, total_size.prod()*3),

	nu_tensor_op      (total_size.prod()*3, total_size.prod()*3),
	tensor_divergence (total_size.prod()*3, total_size.prod()*9),

	charge_density (Field::Zero(total_size.prod()  )),
	current_density(Field::Zero(total_size.prod()*3)),
	pressure       (Field::Zero(total_size.prod()*9)),
	dielectric     (Field::Zero(total_size.prod()*9)),

	E    (Field::Zero(total_size.prod()*3)),
	E_old(Field::Zero(total_size.prod()*3)),
	B    (Field::Zero(total_size.prod()*3)),
	B_old(Field::Zero(total_size.prod()*3)),

	potential(Field::Zero(total_size.prod()  )),
	div_P    (Field::Zero(total_size.prod()*3)),

	out("energy_output"),
	hdf_out(comm, dx, dt, time, timestep),
	
	ionization(comm,
		parts,
		FLAVOR_PROTON_JET, parameters.at("vi_0"), parameters.at("Ti_x"), parameters.at("Ti_y"), parameters.at("Ti_z"), 
		FLAVOR_ELECTRON_JET, parameters.at("Tl_x"), parameters.at("Tl_y"), parameters.at("Tl_z"),
		parameters.at("steps_of_injection"), parameters.at("num_P_jet"),
		parameters.at("r_larm"),
		parameters.at("dr"),
		dx
		),

	error_count(0) {

	//fail if one dimension is too small for TSC
	if( (Nx < 5 && Nx != 1) ||
	    (Ny < 5 && Ny != 1) ||
	    (Nz < 5 && Nz != 1) ) {
		cout << "Dimensions seem to be nonsense with TSC!" << endl;
		exit(1);
	}

	if(comm.rank() == 0 && debug)
		cout << "Init simulation" << endl;
	init_parameters();
	if(comm.rank() == 0 && debug)
		cout << "Init done" << endl;

	//initialize the differential operator sparse matrices
	if(comm.rank() == 0 && debug) {
		cout << "Operators:" << endl;
	}
	stencil_identity();

	stencil_gradient();
	if(comm.rank() == 0 && debug)
		cout << "Gradient done" << endl;
	stencil_divergence();
	if(comm.rank() == 0 && debug)
		cout << "Divergence done" << endl;
	stencil_tensor_divergence();
	if(comm.rank() == 0 && debug)
		cout << "Tensordivergence done" << endl;
	stencil_curl_B();
	stencil_curl_E();
	if(comm.rank() == 0 && debug)
		cout << "Curls done" << endl;

	scalar_laplace = divergence * gradient;
	scalar_laplace.prune(1. / (dx * dx), 1e-6 / (dx * dx));

	//the following basically calculates the Kronecker sum of 3 scalar laplace operators to get the vector version
	vector<Eigen::Triplet<double>> trips;
	for (int k=0; k<scalar_laplace.outerSize(); ++k) {
		for (Operator::InnerIterator it(scalar_laplace,k); it; ++it) {
			//get all 3 indices back
			int cx,cy,cz,rx,ry,rz;
			resindg(it.col(), cx, cy, cz);
			resindg(it.row(), rx, ry, rz);

			//combine three scalar laplace along each axis
			trips.push_back(Eigen::Triplet<double>(vindg(rx,ry,rz,0), vindg(cx,cy,cz,0), it.value()));
			trips.push_back(Eigen::Triplet<double>(vindg(rx,ry,rz,1), vindg(cx,cy,cz,1), it.value()));
			trips.push_back(Eigen::Triplet<double>(vindg(rx,ry,rz,2), vindg(cx,cy,cz,2), it.value()));
		}
	}
	vector_laplace.setFromTriplets(trips.begin(), trips.end());

	//init_sources();
	init_fields();
	init_particles();

	if(comm.rank() == 0 && debug) {
		cout << "Initial output" << endl;
	}

	//add name, frequency, field, and component to the output lists
	hdf_out.add_scalar_output   ("q", parameters.at("out_q"  ), charge_density    );
	hdf_out.add_component_output("E", parameters.at("out_E_x"), E              , 0);
	hdf_out.add_component_output("E", parameters.at("out_E_y"), E              , 1);
	hdf_out.add_component_output("E", parameters.at("out_E_z"), E              , 2);
	hdf_out.add_component_output("B", parameters.at("out_B_x"), B              , 0);
	hdf_out.add_component_output("B", parameters.at("out_B_y"), B              , 1);
	hdf_out.add_component_output("B", parameters.at("out_B_z"), B              , 2);
	hdf_out.add_component_output("j", parameters.at("out_j_x"), current_density, 0);
	hdf_out.add_component_output("j", parameters.at("out_j_y"), current_density, 1);
	hdf_out.add_component_output("j", parameters.at("out_j_z"), current_density, 2);

	//add particle output to output lists
	hdf_out.add_particle_output(parameters.at("out_p"), parts);

	//output everything we need
	hdf_out.output();
	energy_output();

	for(int i = 0; i < NEWTON_MAX_ITERS; i++)
		iteration_histogram[i] = 0;
	timestep++;
	time += dt;
}
