// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#include <iomanip>

#include "simulation.h"

using namespace std;

using Eigen::Vector3d;

//calculate (diagnostic) energy information
void Simulation::energy_output() {
	double E_field = 0., E_E = 0., E_B = 0.;
	Vector3d E_xyz(0., 0., 0.), B_xyz(0., 0., 0.), P_xyz(0., 0., 0.);

	//sum up field energy density for each cell
	for(int z = 0; z < Nz; z++) {
		for(int y = 0; y < Ny; y++) {
			for(int x = 0; x < Nx; x++) {
				Vector3d E_tmp(E[vindg(x, y, z, 0)], E[vindg(x, y, z, 1)], E[vindg(x, y, z, 2)]);
				Vector3d B_tmp(B[vindg(x, y, z, 0)], B[vindg(x, y, z, 1)], B[vindg(x, y, z, 2)]);

				E_E += SI::eps0/2.     * E_tmp.dot(E_tmp);
				E_B += 1./(2.*SI::mu0) * B_tmp.dot(B_tmp);

				E_xyz += SI::eps0/2.     * E_tmp.cwiseProduct(E_tmp);
				B_xyz += 1./(2.*SI::mu0) * B_tmp.cwiseProduct(B_tmp);
			}
		}
	}
	E_field = E_B + E_E;

	//sum up kinetic energy for each particle
	double E_kin = 0.;
	for(vector<Particle>::size_type i = 0; i < parts.size(); i++) {
		Vector3d u(parts[i].get_u());
		if(u.dot(u) < 0.075 * SI::c) {
			E_kin += 0.5 * parts[i].get_m() * parts[i].get_density() * u.dot(u);
		} else {
			E_kin += (parts[i].get_gamma() - 1.0) * parts[i].get_m() * SI::c * SI::c * parts[i].get_density();
		}
		P_xyz += 0.5 * parts[i].get_m() * parts[i].get_density() * u.cwiseProduct(u);
	}

	//reduce for all ranks
	double tmp;
	comm.global_sum(E_kin, tmp);
	E_kin = tmp * cell_vol;
	comm.global_sum(E_field, tmp);
	E_field = tmp * cell_vol;
	comm.global_sum(E_B, tmp);
	E_B = tmp * cell_vol;
	comm.global_sum(E_E, tmp);
	E_E = tmp * cell_vol;

	Vector3d vtmp;
	comm.global_sum(P_xyz[0], vtmp[0], 3);
	P_xyz = vtmp * cell_vol;
	comm.global_sum(E_xyz[0], vtmp[0], 3);
	E_xyz = vtmp * cell_vol;
	comm.global_sum(B_xyz[0], vtmp[0], 3);
	B_xyz = vtmp * cell_vol;

	//write to text file
	if(comm.rank() == 0) {
		if(timestep == 0) {
			out << "Timestep\tTime\tT-Energy\tP-Energy\tEB-Energy\tB-Energy\tE-Energy\tP_x\tP_y\tP_z\tB_x\tB_y\tB_z\tE_x\tE_y\tE_z" << endl;
		}
		out << timestep << "\t" << time << "\t" << E_kin+E_field
		    << "\t" << E_kin << "\t" << E_field << "\t" << E_B << "\t" << E_E
		    << "\t" << P_xyz[0] << "\t" << P_xyz[1] << "\t" << P_xyz[2]
		    << "\t" << B_xyz[0] << "\t" << B_xyz[1] << "\t" << B_xyz[2]
		    << "\t" << E_xyz[0] << "\t" << E_xyz[1] << "\t" << E_xyz[2]
		    << endl;
	}
}
