#include "ionize_particles.h"

#include "simulation.h"
#include "util.h"
#include <cmath> // for nextafter(...) isinf(double)
#include <random>

#define LINEAR_TIME_PROFILE		true
#define BEAM_IS_NEUTRAL 		true

const int NOT_UNIQUE_SEED = 1;


void
Ionize_particles::execute(size_t t)
{
 	// First of all we need to check if there any coordinates
	// in the simulation domain from the ionization area  
	if (!part_of_area_in_simulation_domain()) return;

    static const double mi = Particle::p[ions_flav].m / SI::me;
	static const double ml = Particle::p[lost_flav].m / SI::me;
	static const double pi_0 = mi * ui_0;
	static const double pl_0 = ml * ul_0;

	// To close the interval of the real distribution we use std::nextafter(double from, double to)
	static std::mt19937_64 eng(0);
	static auto distribution = std::uniform_real_distribution(0., std::nextafter(1., 2.));
	
	Tag tag;
	tag.guid = 1; // ???
	tag.id_in_cell = 0; // ???
	tag.population = POP_JET;

	int err = 0;
	for (size_t i = 0; i < amount_of_particles_to_load(t) + err; ++i) {
		
		Eigen::Vector3d r;
		r = pick_point_on_annulus();
		
		// To parallelize the process of ionization we skip wrong r-s
		if (!picked_point_in_simulation_domain(r)) continue;
	
		if (distribution(eng) > uniform_probability(r)) {
			err++;
			continue;
		}

		// if we are in the simulation domain, then we must substract the offset
		r[0] -= comm.get_cell_offset(0);
		r[1] -= comm.get_cell_offset(1);
		r[2] -= comm.get_cell_offset(2);

		tag.start_cell_index = ind(floor(r[0]), floor(r[1]), floor(r[2]), 0,
			comm.get_local_size(0), comm.get_local_size(1), comm.get_local_size(2), 1);

		Eigen::Vector3d ui;
		ui = pick_annular_impulse(r, pi_0, mi, Ti) / mi;
		ui /= sqrt(1. + ui.dot(ui));

		tag.flav = ions_flav;
		list_of_particles.push_back(Particle(r, ui, tag));

		#if BEAM_IS_NEUTRAL
		{
			Eigen::Vector3d ul;
			ul = pick_annular_impulse(r, pl_0, ml, Tl) / ml;
			ul /= sqrt(1. + ul.dot(ul));

			tag.flav = lost_flav;
			list_of_particles.push_back(Particle(r, ul, tag));
		}
		#endif
	}
}


bool
Ionize_particles::part_of_area_in_simulation_domain()
{
	static const double r0 = r_larm + dr;

	return (
		comm.get_cell_offset(0) + comm.get_local_size(0)
								> 0.5 * comm.get_global_cell_size(0) - r0 && // left
		comm.get_cell_offset(0) < 0.5 * comm.get_global_cell_size(0) + r0 && // right
		comm.get_cell_offset(1) + comm.get_local_size(1)
								> 0.5 * comm.get_global_cell_size(1) - r0 && // lower
		comm.get_cell_offset(1) < 0.5 * comm.get_global_cell_size(1) + r0);	 // upper
}


bool
Ionize_particles::picked_point_in_simulation_domain(const Eigen::Vector3d& r)
{
	return (
		r[0] > comm.get_cell_offset(0) && r[0] < comm.get_cell_offset(0) + comm.get_local_size(0) &&
		r[1] > comm.get_cell_offset(1) && r[1] < comm.get_cell_offset(1) + comm.get_local_size(1));
}


Eigen::Vector3d
Ionize_particles::pick_point_on_annulus()
{
	// We'll use the same seed on all ranks to provide a proper particle distribution
	static std::mt19937_64 eng(NOT_UNIQUE_SEED);

	// To close the interval of the real distribution we use std::nextafter(double from, double to)
	static auto distribution = std::uniform_real_distribution(0., std::nextafter(1., 2.));

	double ra = r_larm - dr;
	double rb = r_larm + dr;
	double r0 = sqrt(ra * ra + (rb * rb - ra * ra) * distribution(eng));
	
	double phi = 2. * M_PI * distribution(eng);
	
	Eigen::Vector3d r;

	r[0] = 0.5 * comm.get_global_cell_size(0) + r0 * cos(phi) / dx;
	r[1] = 0.5 * comm.get_global_cell_size(1) + r0 * sin(phi) / dx;
	r[2] = 0.5 * comm.get_global_cell_size(2);

	return r;
} 


Eigen::Vector3d
Ionize_particles::pick_annular_impulse(
	const Eigen::Vector3d& r, double p0,
	double mass, const Eigen::Vector3d& temperature)
{
	double center_x = 0.5 * comm.get_global_cell_size(0);
	double center_y = 0.5 * comm.get_global_cell_size(1);
	double r0 = sqrt(
		((comm.get_cell_offset(0) + r[0]) - center_x) * ((comm.get_cell_offset(0) + r[0]) - center_x) 
	  + ((comm.get_cell_offset(1) + r[1]) - center_y) * ((comm.get_cell_offset(1) + r[1]) - center_y)
	  );
	
	Eigen::Vector3d p;
	
	p[0] = p0 * ( +((comm.get_cell_offset(1) + r[1]) - center_y) / r0 )
		 + temperature_impulse(temperature[0], mass);

	p[1] = p0 * ( -((comm.get_cell_offset(0) + r[0]) - center_x) / r0 )
		 + temperature_impulse(temperature[1], mass);

	p[2] = temperature_impulse(temperature[2], mass);

	return p;
}


double
Ionize_particles::temperature_impulse(double temperature, double mass)
{
	// Here we can use unique seed, in won't produce any error
	static std::mt19937_64 eng(comm.rank());
	static auto uniform = std::uniform_real_distribution<double>(0., std::nextafter(1., 2.));
	static auto normal = std::normal_distribution<double>(0., sqrt(mass * temperature / 511.0e3));

	return sin(2. * M_PI * uniform(eng)) * sqrt(fabs(normal(eng)));
}


double
Ionize_particles::uniform_probability(const Eigen::Vector3d& r) { return 1.; }


size_t
Ionize_particles::amount_of_particles_to_load(size_t timestep)
{
	static const size_t totalJetProtons = static_cast<size_t>(std::ceil(
		protonJetNumber * M_PI *
		+ ( (r_larm + dr) * (r_larm + dr)
		-   (r_larm - dr) * (r_larm - dr) ) / (dx * dx)
	));

	// плохой вариант
	#if LINEAR_TIME_PROFILE
	{
		static std::vector<size_t> protonPerStep(steps_of_injection - 1, 0);
		static bool arrayIsLoaded = false;

		static std::mt19937_64 eng(NOT_UNIQUE_SEED);
		// используем левую границу для старта инжекции в термализованную плазму
		static std::uniform_int_distribution distribution(1260, (int)steps_of_injection - 1);
		
		if (!arrayIsLoaded)
		{
			for (size_t i = 0u; i <= totalJetProtons; ++i) {
				protonPerStep[distribution(eng)] += 1;
			}

			arrayIsLoaded = true;
		}
		
		return protonPerStep[timestep];
	}
	#endif
}
		
