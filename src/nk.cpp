// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#include <Eigen/IterativeLinearSolvers>

#include "constants.h"
#include "simulation.h"

using namespace std;

using Eigen::Vector3d;
using Eigen::Matrix3d;

//outer iteration of Newton-Krylov scheme
Vector3d Simulation::nk_solve(Particle& p) {
	//remember the current particle
	p_n = p;
	//velocity (outer iteration)
	u_outer = p_n.get_u();

	//velocity (inner iteration)
	Vector3d uu(p_n.get_u());

	//compute the first residual
	F0 = compute_r(u_outer, Vector3d::Zero(), p.get_f());
	//we want to reduce the residual below a certain tolerance value
	const double tau = F0.norm() * NEWTON_TOLERANCE + NEWTON_TOLERANCE;
	const double tau_critical = F0.norm() * 1e-2 + NEWTON_TOLERANCE;
	//outer iteration (fixed number of maximum iterations)
	int i;
	for(i = 0; i < NEWTON_MAX_ITERS; i++) {
		int iters = GMRES_MAX_ITERS;
		double tol_error = GMRES_TOLERANCE;

		//get new velocity from inner iteration
		int non_conv = nk_iterate(uu, iters, tol_error, p.get_f());

		//if something went wrong, warn user and possibly bail out
		if(non_conv || !valid_velocity(uu)) {
			//if not fatal, use the bad step anyway, outer iteration might still converge
			//if fatal, use the old velocity instead of aborting or crashing with a NaN (might happen and is not critical if it happens only sometimes)
			error_count++;
			if(!valid_velocity(uu)) {
				cout << "WW: Particle GMRES failure! Bailing!" << endl;
				cout << "Type: " << static_cast<int>(p_n.get_f()) << hex << " ID: " << p_n.get_t() << dec << endl;
				cout << uu.transpose() << endl;
				cout << p_n.get_u().transpose() << endl;
				return(p_n.get_u());
			}
		}

		//start a primitive line search
		//decide if we want to use the step we got from the inner iteration or if we want to use a shorter step that might lead to a better residual
		double s = 2.0;
		Vector3d Fd;
		double fmin = 1e30;
		double smax = 0.;
		do {
			s *= 0.5;
			//get residual for smaller step
			Fd = compute_r(u_outer-s*uu, Vector3d::Zero(), p.get_f());

			//is it better than the best one we encountered?
			if(Fd.norm() < fmin) {
				fmin = Fd.norm();
				smax = s;
			}
		} while(Fd.norm() > (1. - 1e-4*s) * F0.norm() && s > 1e-4); //until s is too small or until we are satisfied

		//take the step
		u_outer -= smax*uu;
		//get residual for the new velocity
		F0 = compute_r(u_outer, Vector3d::Zero(), p.get_f());

		//we are done if current residual is lower than our tolerance
		if(F0.norm() < tau)
			break;
	}
	//log how often we had to iterate in outer iteration (diagnostics)
	iteration_histogram[i]++;

	//residual not decreased sufficiently -> warn user, bail out for bad solutions
	if(F0.norm() > tau || !valid_velocity(u_outer)) {
		cout << "WW! Newton failure!" << endl;
		cout << "Type: " << static_cast<int>(p_n.get_f()) << hex << " ID: " << p_n.get_t() << dec << endl;
		cout << F0.transpose() << endl;
		cout << tau << endl;
		cout << u_outer.transpose() << endl;
		cout << p_n.get_u().transpose() << endl;
		error_count++;

		if(F0.norm() > tau_critical || !valid_velocity(u_outer)) {
			cout << "Critical! Bailing!" << endl;
			return(p_n.get_u());
		}
	}

	return(u_outer);
}

//compute the residual for the particle movement equation
Vector3d Simulation::compute_r(const Vector3d& u, const Vector3d& dist, uint8_t flav) const {
	const Vector3d uu(u + dist);
	const double gamma_new = sqrt(1. + uu.dot(uu)/(SI::c*SI::c));

	const Vector3d v_half = (p_n.get_u() + uu) / (p_n.get_gamma() + gamma_new);
	const Vector3d r_half = p_n.get_r() + v_half * dt/2. / dx;

	Vector3d E_loc, B_loc_old;
	get_local_fields(E, B_old, r_half, E_loc, B_loc_old, flav);

	const Vector3d residual = (uu - p_n.get_u())/dt - p_n.get_q()/p_n.get_m() * (E_loc + v_half.cross(B_loc_old));

	return(residual);
}

//three helper functions
void Simulation::nk_generate_plane_rotation(const double& dx, const double& dy, double& cs, double& sn) const {
	if(dy == 0.0) {
		cs = 1.0;
		sn = 0.0;
	} else if(abs(dy) > abs(dx)) {
		const double tmp = dx / dy;
		sn = 1.0 / sqrt(1.0 + tmp*tmp);
		cs = tmp * sn;
	} else {
		const double tmp = dy / dx;
		cs = 1.0 / sqrt(1.0 + tmp*tmp);
		sn = tmp * cs;
	}
}

void Simulation::nk_update(Vector3d& x, int k, const RestartMatrix& h, const RestartVector& s, const Restart3DMatrix& v) const {
	Eigen::VectorXd y = h.block(0, 0, k+1, k+1).fullPivHouseholderQr().solve(s.block(0, 0, k+1, 1));
	for(int i = 0; i <= k; i++) {
		Vector3d tmp(v.block(0, 0, k+1, 3).row(i));
		x += tmp * y(i);
	}
}

void Simulation::nk_apply_plane_rotation(double& dx, double& dy, const double& cs, const double& sn) const {
	const double tmp  =  cs * dx + sn * dy;
	dy = -sn * dx + cs * dy;
	dx = tmp;
}

//inner iteration of Newton-Krylov scheme (a modified GMRES scheme)
int Simulation::nk_iterate(Vector3d& x, int& max_iter, double& tol, uint8_t flav) {
	RestartMatrix H = RestartMatrix::Zero();
	double resid;
	int i, j = 1, k;
	RestartVector s, cs, sn;
	s.setConstant (std::numeric_limits<double>::quiet_NaN());
	cs.setConstant(std::numeric_limits<double>::quiet_NaN());
	sn.setConstant(std::numeric_limits<double>::quiet_NaN());

	const Vector3d b(F0);

	double normb = b.norm();
	if(normb == 0.0)
		normb = 1.;

	const double xnorm = x.norm();
	Vector3d r;
	if(xnorm > 0.) {
		//decide on the epsilon for calculating the numerical derivative of the residual function
		double epsnew = DERIVATION_DELTA / xnorm;
		if(u_outer.norm() > 0.)
			epsnew = epsnew * u_outer.norm();
		//calculate the numerical derivative
		const Vector3d F1 = compute_r(u_outer, x * epsnew, flav);
		const Vector3d dirder = (F1 - F0) / epsnew;
		r = b - dirder;
	} else if(xnorm == 0.) {
		r = b;
	} else {
		return(1);
	}

	double beta = r.norm();

	if((resid = r.norm() / normb) <= tol) {
		//immediate convergence
		tol = resid;
		max_iter = 0;
		return 0;
	}

	Restart3DMatrix v = Restart3DMatrix::Zero();

	while(j <= max_iter) {
		v.row(0) = r / beta;
		s.setZero();
		s(0) = beta;

		for(i = 0; j <= max_iter; i++, j++) {
			const double xnorm = v.row(i).norm();
			Vector3d w;
			if(xnorm > 0.) {
				//decide on the epsilon for calculating the numerical derivative of the residual function
				double epsnew = DERIVATION_DELTA / xnorm;
				if(u_outer.norm() > 0.)
					epsnew = epsnew * u_outer.norm();
				//calculate the numerical derivative
				const Vector3d F1(compute_r(u_outer, v.row(i) * epsnew, flav));
				w = (F1 - F0) / epsnew;
			} else if(xnorm == 0.) {
				w = Vector3d::Zero();
			} else {
				return(1);
			}

			//GMRES algorithm
			for(k = 0; k <= i; k++) {
				H(k, i) = w.dot(v.row(k));
				w -= H(k, i) * v.row(k);
			}
			H(i+1, i) = w.norm();
			v.row(i+1) = w / H(i+1, i);

			for(k = 0; k < i; k++)
				nk_apply_plane_rotation(H(k,i), H(k+1,i), cs(k), sn(k));

			nk_generate_plane_rotation(H(i,i), H(i+1,i), cs(i), sn(i));
			nk_apply_plane_rotation(H(i,i), H(i+1,i), cs(i), sn(i));
			nk_apply_plane_rotation(s(i), s(i+1), cs(i), sn(i));

			if((resid = abs(s(i+1)) / normb) < tol) {
				//convergence
				nk_update(x, i, H, s, v);
				tol = resid;
				max_iter = j;
				return 0;
			}
		}
		nk_update(x, i - 1, H, s, v);

		const double xnorm = x.norm();
		Vector3d r;
		if(xnorm > 0.) {
			//decide on the epsilon for calculating the numerical derivative of the residual function
			double epsnew = DERIVATION_DELTA / xnorm;
			if(u_outer.norm() > 0.)
				epsnew = epsnew * u_outer.norm();
			//calculate the numerical derivative
			const Vector3d F1 = compute_r(u_outer, x * epsnew, flav);
			const Vector3d dirder = (F1 - F0) / epsnew;
			r = b - dirder;
		} else if(xnorm == 0.) {
			r = b;
		} else {
			return(1);
		}

		beta = r.norm();
		if((resid = beta / normb) < tol) {
			//convergence
			tol = resid;
			max_iter = j;
			return 0;
		}
	}

	//failure
	tol = resid;
	return 1;
}
