// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#include "simulation.h"

using namespace std;

//read in config file, stow key-value pairs in hash table
std::unordered_map<std::string, double> Simulation::load_parameters() const {
	std::unordered_map<std::string, double> parameters;
	ifstream in("config");

	string buf_key, buf_val;
	getline(in, buf_key, '\n');

	while(in.good()) {
		getline(in, buf_key, '=');

		if(in.eof()) break;
		if(buf_key.size() == 0) continue;

		getline(in, buf_val);

		parameters.insert({buf_key, stod(buf_val)});
		buf_key = buf_val = "";
	}

	return(parameters);
}
