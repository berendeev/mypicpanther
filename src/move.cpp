// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#include "simulation.h"

using namespace std;

using Eigen::Vector3d;

//move all particles
void Simulation::move_particles() {
	//for each particle we have
	for(vector<Particle>::size_type i = 0; i < parts.size(); i++) {
		//get the advanced velocity from Newton-Krylov solver
		const Vector3d u_new = nk_solve(parts[i]);

		//actually move the particle
		parts[i].move(u_new);
	}

	//give away particles we are no longer responsible for
	comm.move_particles(parts);
	//correct the locations of particles we get from somewhere
	comm.particle_boundaries(parts);

	sort(parts.begin(), parts.end(),
		[&, this](const Particle& a, const Particle& b) -> bool {
			return(sindg(a.get_r(0), a.get_r(1), a.get_r(2)) < sindg(b.get_r(0), b.get_r(1), b.get_r(2)));
		}
	);
}
