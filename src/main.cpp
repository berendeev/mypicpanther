// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#include "simulation.h"
#include <unistd.h> // for sleep

using namespace std;

//Main function simply hands off control to the Simulation class
int main(int argc, char* argv[])
{
	#if MPI_DEBUG
	{
		int i = 0;
		while (i == 0)
			sleep(5);
	}
	#endif

	Simulation sim(argc, argv);

	sim.main_loop();

	return EXIT_SUCCESS;
}
