// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#include <chrono>
#include <algorithm>

#include "simulation.h"

using namespace std;

using Eigen::Vector3d;
using Eigen::Matrix3d;
using Eigen::Map;

//accumulate source terms from particle data (Yee)
void Simulation::calc_sources() {
	//zero out previous data
	current_density.setZero();
	charge_density.setZero();

	pressure.setZero();

	dielectric.setZero();

	double formx0 [5];
	double formx05[5];
	double formy0 [5];
	double formy05[5];
	double formz0 [5];
	double formz05[5];

	//for each particle we have
	for(vector<Particle>::size_type i = 0; i < parts.size(); i++) {
		const Vector3d& r = parts[i].get_r();

		//interpolate fields to particle position
		Vector3d E_loc, B_loc;
		get_local_fields(E, B, r, E_loc, B_loc, parts[i].get_f());

		Vector3d j;
		Matrix3d P;
		Matrix3d nu;

		//get particle data from particle itself
		parts[i].get_moments(THETA, E_loc, B_loc, j, P, nu);

		//calculate form factors for particle
		const int l = static_cast<int>(floor(r[0]))-2;
		const int m = static_cast<int>(floor(r[1]))-2;
		const int n = static_cast<int>(floor(r[2]))-2;

		for(int aa = 0; aa < 5; aa++) {
			formx0 [aa] = (Nx > 1) ? weight(r[0], l+aa     ) : 1;
			formx05[aa] = (Nx > 1) ? weight(r[0], l+aa, 0.5) : 1;
			formy0 [aa] = (Ny > 1) ? weight(r[1], m+aa     ) : 1;
			formy05[aa] = (Ny > 1) ? weight(r[1], m+aa, 0.5) : 1;
			formz0 [aa] = (Nz > 1) ? weight(r[2], n+aa     ) : 1;
			formz05[aa] = (Nz > 1) ? weight(r[2], n+aa, 0.5) : 1;
		}

		//for all cells touched by TSC particle
		for(int aa = 0; aa < min(5,Nx); aa++) {
			for(int bb = 0; bb < min(5,Ny); bb++) {
				for(int cc = 0; cc < min(5,Nz); cc++) {
					//accumulate charge density, current density, dielectric tensor, pressure for Yee scheme
					charge_density[sindg(l+aa,m+bb,n+cc)] += parts[i].get_rho() * formx0[aa] * formy0[bb] * formz0[cc];

					current_density[vindg(l+aa,m+bb,n+cc, 0)] += j[0] * formx05[aa] * formy0 [bb] * formz0 [cc];
					current_density[vindg(l+aa,m+bb,n+cc, 1)] += j[1] * formx0 [aa] * formy05[bb] * formz0 [cc];
					current_density[vindg(l+aa,m+bb,n+cc, 2)] += j[2] * formx0 [aa] * formy0 [bb] * formz05[cc];

					Map<Vector3d> dielec0(&dielectric[tindg(l+aa,m+bb,n+cc, 0)]);
					dielec0 += nu.row(0) * formx05[aa] * formy0 [bb] * formz0 [cc];
					Map<Vector3d> dielec1(&dielectric[tindg(l+aa,m+bb,n+cc, 3)]);
					dielec1 += nu.row(1) * formx0 [aa] * formy05[bb] * formz0 [cc];
					Map<Vector3d> dielec2(&dielectric[tindg(l+aa,m+bb,n+cc, 6)]);
					dielec2 += nu.row(2) * formx0 [aa] * formy0 [bb] * formz05[cc];


					pressure[tindg(l+aa,m+bb,n+cc, 0)] += P(0, 0) * formx0 [aa] * formy0 [bb] * formz0 [cc];
					pressure[tindg(l+aa,m+bb,n+cc, 1)] += P(0, 1) * formx05[aa] * formy05[bb] * formz0 [cc];
					pressure[tindg(l+aa,m+bb,n+cc, 2)] += P(0, 2) * formx05[aa] * formy0 [bb] * formz05[cc];

					pressure[tindg(l+aa,m+bb,n+cc, 3)] += P(1, 0) * formx05[aa] * formy05[bb] * formz0 [cc];
					pressure[tindg(l+aa,m+bb,n+cc, 4)] += P(1, 1) * formx0 [aa] * formy0 [bb] * formz0 [cc];
					pressure[tindg(l+aa,m+bb,n+cc, 5)] += P(1, 2) * formx0 [aa] * formy05[bb] * formz05[cc];

					pressure[tindg(l+aa,m+bb,n+cc, 6)] += P(2, 0) * formx05[aa] * formy0 [bb] * formz05[cc];
					pressure[tindg(l+aa,m+bb,n+cc, 7)] += P(2, 1) * formx0 [aa] * formy05[bb] * formz05[cc];
					pressure[tindg(l+aa,m+bb,n+cc, 8)] += P(2, 2) * formx0 [aa] * formy0 [bb] * formz0 [cc];
				}
			}
		}
	}

	//give everything sitting in ghost cells to our neighbors
	comm.move_sources(charge_density, 1);
	comm.move_sources(current_density, 3);
	comm.move_sources(pressure, 9);
	comm.move_sources(dielectric, 9);

	//make a vector operator out of the dielectric tensor
	stencil_nu_tensor_op();

	//precompute and distribute vector divergence of pressure
	div_P = tensor_divergence * pressure;
	comm.move_fields(div_P, 3);

	//optional smoothing of charge_density
	if(smooth_charge > 0) {
		smooth(charge_density, 1);
	}
}