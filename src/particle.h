// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#pragma once

#ifndef PARTICLE_H
#define PARTICLE_H

#include <cstdint>

#include <Eigen/Dense>

#include "constants.h"
#include "util.h"

static const uint8_t AMOUNT_OF_SORTS		= 4;
static const uint8_t FLAVOR_ELECTRON 		= 0;
static const uint8_t FLAVOR_PROTON   		= 1;
static const uint8_t FLAVOR_ELECTRON_JET 	= 2;
static const uint8_t FLAVOR_PROTON_JET 		= 3;
// static const uint8_t FLAVOR_POSITRON = 2;

static const uint8_t POP_BG  = 0;
static const uint8_t POP_JET = 1;

//properties for each particle flavor
struct Properties {
	double m; //mass of non-macro particle (in kg)
	double q; //charge of non-macro particle (in C)
	double beta_dt; //beta / dt (in C/kg)
	double density; //density of non-macro particle (in 1/m^3)
};

//ID tag to identify a particle
union Tag {
	uint64_t guid;
	struct {
		uint8_t flav; //Particle flavor, identifies protons, electrons, positrons! Used to index Properties array!
		uint8_t population;
		uint16_t id_in_cell;
		uint32_t start_cell_index;
	};
};

//Particle class
class Particle {
	public:
		static Properties p[];
	private:
		static double dt;
		static double dx;

		Eigen::Vector3d r; //position in units of dx, relative to our local origin
		Eigen::Vector3d u; //velocity in \gamma*v (in m/s)

		Tag id; //particle ID, should be made unique

	public:
		Particle() {}
		Particle(const Eigen::Vector3d& position, const Eigen::Vector3d& velocity, Tag tag);

		void move(const Eigen::Vector3d& u_new);

		Eigen::Matrix3d get_alpha(const Eigen::Vector3d& E_old, const Eigen::Vector3d& B) const;
		void get_moments(double theta, const Eigen::Vector3d& E, const Eigen::Vector3d& B, Eigen::Vector3d& j, Eigen::Matrix3d& P, Eigen::Matrix3d& nu) const;
		const double& get_q() const {return(p[id.flav].q);}
		const double& get_m() const {return(p[id.flav].m);}
		double get_beta() const {return(dt*p[id.flav].beta_dt);}
		double get_density() const {return(p[id.flav].density);}
		double get_rho() const {return(p[id.flav].q*p[id.flav].density);}
		inline double get_gamma() const {return(sqrt(1. + u.dot(u)/(SI::c*SI::c)));}
		const Eigen::Vector3d& get_r() const {return(r);}
		const double& get_r(unsigned int c) const {return(r[c]);}
		inline Eigen::Vector3d get_v() const {return(u/get_gamma());}
		const Eigen::Vector3d& get_u() const {return(u);}
		uint8_t get_f() const {return(id.flav);}
		uint64_t get_t() const {return(id.guid);}
		uint8_t get_population() const {return id.population;}

		void set_r(const Eigen::Vector3d& r1) {r = r1;}
		void set_r(double rc, int c) {r[c] = rc;}
		void set_u(const Eigen::Vector3d& u1) {u = u1;}

		void correct_r(unsigned int c, double delta) {r[c] += delta;}

		static void set_dt(double dt_) {dt = dt_;}
		static void set_dx(double dx_) {dx = dx_;}
};

#endif
