// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#pragma once

#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H

#include <mpi.h>

#include <Eigen/Dense>

#include "particle.h"
#include "util.h"

//MPI communication class
class Communicator {
	private:
		static const int LEFT = 0;
		static const int RIGHT = 1;

		static const int PARTICLE_COMM_NUM = 0;
		static const int PARTICLE_COMM_DAT = 32;
		static const int FIELD_COMM = 64;

		Eigen::Vector3i num_cpus; //how many ranks (CPUs) along each axis
		Eigen::Vector3i global_cell_size; //size of the global simulation (in cells)
		Eigen::Vector3i ghost_cells; //how many ghost cells in each direction
		Eigen::Vector3i periodic; //is space periodic along this axis?

		int error_state;
		MPI_Comm comm; //actual MPI communicator

		int num_cpus_total; //total number of individual ranks
		int neighbors[3][2]; //our neighbors to the left/right along each axis
		int my_rank; //our own rank

		Eigen::Vector3i cpu_coords; //our coordinates in CPU space

		Eigen::Vector3i cell_offset; //our local offset in global cell space

		Eigen::Vector3i sim_size; //our local simulation size
		Eigen::Vector3i total_size; //local sim size + ghost_cells

		Eigen::Vector3i outer_min; //minimum coordinate of simulation + ghost_cells
		Eigen::Vector3i sim_min; //minimum coordinate of simulation
		Eigen::Vector3i inner_min; //minimum coordinate of simulation - ghost_cells
		Eigen::Vector3i outer_max; //maximum...
		Eigen::Vector3i sim_max; //maximum...
		Eigen::Vector3i inner_max; //maximum

		//helper indexing routine taking into account ghost cells
		inline int indg(int x, int y, int z, int c, int Nx, int Ny, int Nz, int Nc) {
			return(ind(x + ghost_cells[0], y + ghost_cells[1], z + ghost_cells[2], c, Nx, Ny, Nz, Nc));
		}

	public:
		Communicator(int argc, char* argv[], Eigen::Vector3i& global_cell_size_, Eigen::Vector3i& ghost_cells_, Eigen::Vector3i& num_cpus_);
		~Communicator();

		Eigen::Vector3i get_local_size() const { return sim_size;}

		int get_local_size(int c) 		const	{ return sim_size[c];			}
		int get_total_size(int c) 		const	{ return total_size[c];			}
		int get_cell_offset(int c)		const	{ return cell_offset[c];		}
		int get_global_cell_size(int c) const 	{ return global_cell_size[c];	}
		int get_ghost_cells(int c)		const	{ return ghost_cells[c];		}
		

		int rank() const {return(my_rank);}

		void gather_quantities(const Field& src, Field& dest, const Eigen::Vector3i& start, const Eigen::Vector3i& end, int component_size);
		void scatter_quantities(const Field& src, Field& dest, const Eigen::Vector3i& start, const Eigen::Vector3i& end, int component_size);
		void scatter_add_quantities(const Field& src, Field& dest, const Eigen::Vector3i& start, const Eigen::Vector3i& end, int component_size);

		void move_particles(std::vector<Particle>& parts);
		void particle_boundaries(std::vector<Particle>& parts) const;

		void move_fields(Field& field, int component_size);
		void move_sources(Field& field, int component_size);

		void global_sum(double& in, double& out, int components=1) {
			error_state = MPI_Reduce(&in, &out, components, MPI_DOUBLE, MPI_SUM, 0, comm);
		}

		MPI_Comm& get_communicator() {return(comm);}

		void barrier() {MPI_Barrier(comm);}
};

#endif
