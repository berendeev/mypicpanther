

import numpy as np
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib.colors as col
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import gridspec
from matplotlib import rc
import struct

#колормэпы
#https://matplotlib.org/stable/tutorials/colors/colormaps.html

#font = {'family' : 'serif'}
#rc('text', usetex=True)
#rc('text.latex', unicode=True)
#rc('font', family='serif')
#rc('text.latex', preamble=r"\usepackage[utf8]{inputenc}")
#rc('text.latex', preamble=r"\usepackage[T1]{fontenc}")

def ReadDensFile(WorkDir,fname,TimeStep):
	#открыли файл
	DensName=WorkDir+fname+TimeStep
	#в первых двух флоатах записаны размеры сетки (на самом деле уже не актуально)
	file = open(DensName, 'rb')
	buf = file.read(2*4)
	Prop=struct.unpack("ff", buf[:2*4])
	Nr=int(Prop[0])
	Nz=int(Prop[1])
	size=str(int(Nr)*int(Nz))
	#print('dens',Nx,Ny,size)
	ftypeDens="("+str(2)+")f4,("+size+")f4"
	RawData = np.fromfile(DensName, dtype=ftypeDens)
	data=RawData[0][1]
	#Dens={}
	Dens=data.reshape(( Nr, Nz))
	Dens=np.swapaxes(Dens,0,1)

	return Dens

fig = plt.figure(figsize=(16, 9))
gs =gridspec.GridSpec(2,3,width_ratios=[0.25,1,1],height_ratios=[1,0.25])#первый аргумент - вертикальное количество, второй - горизонтальное 

def SubPlot(x,y,fig):
	return fig.add_subplot(gs[y,x])

#корневой каталог для расчёта (где файл параметров)
WorkDir='../Fields/Diag2D/'

#прочесть параметры сетки
NCx=1000
NCy= 1000
dl=0.2
#,Pr1,Pr2=np.loadtxt(WorkDir+'Prop.txt', skiprows=1, unpack=True,usecols=[0,1,2,3,4])
print(NCx,NCy,dl)
#NCx=int(NCx)
#NCy=int(NCy)

MaxField=20#для построения шкалы
#считать поля

size=str(NCx*NCy)
#составить формат файла
#ftype="("+size+")f4,"

#RawData = np.fromfile(WorkDir+'Bx.bin', dtype=ftype)
Bx=ReadDensFile(WorkDir,'FieldBx','000')
#RawData.reshape(( NCy, NCx))
axX=SubPlot(1,0,fig)
#RawData = np.fromfile(WorkDir+'By.bin', dtype=ftype)
By=ReadDensFile(WorkDir,'FieldBy','000')
#RawData.reshape(( NCy, NCx))
axY=SubPlot(2,0,fig)

im=axX.imshow(Bx,cmap='seismic',vmin=-MaxField,vmax=MaxField,origin='low',aspect='auto')
axX.set_title('$B_x$')
axX.set_ylabel('y, cells')
axX.set_xlabel('x, cells')
im=axY.imshow(By,cmap='seismic',vmin=-MaxField,vmax=MaxField,origin='low',aspect='auto')
axY.set_title('$B_y$')
axY.set_ylabel('y, cells')
axY.set_xlabel('x, cells')

cax = fig.add_axes([0.55-0.2, 0.92, 0.4, 0.025])
divider = make_axes_locatable(cax)
#cax = divider.append_axes("top", size="2%", pad=0.4)#свойства colorbar'а
cbar =fig.colorbar(im, cax=cax, orientation='horizontal')
tick_locator = ticker.MaxNLocator(nbins=5)
cbar.locator = tick_locator
# cbar.formatter.set_powerlimits((-1, 5))
#cb.format=ticker.FuncFormatter(fmt)	
cbar.ax.xaxis.set_ticks_position('top')
cbar.update_ticks()

	

#срезы
axXy=SubPlot(0,0,fig)
y=np.linspace(0,NCy,NCy)
#axXy.plot(np.swapaxes(Bx,0,1)[int(NCy/2)],y)
axXy.plot(np.swapaxes(Bx,0,1)[int(NCx/2)],y)
axXy.set_ylabel('y, cells')
axXy.set_xlabel('$B_x$')
axXy.set_xlim(0,MaxField)
axXy.set_title('Поле в центре $NCx/2$')

axXx=SubPlot(1,1,fig)
axXx.plot(Bx[(int(NCy/2))])
axXx.set_xlabel('x, cells')
axXx.set_ylabel('$B_x$')
axXx.set_title('Поле в центре $NCy/2$')
axXx.set_ylim(0,MaxField)


#print('Bx in Probka=',Bx[int(NCy/2)][int(Pr1/dl)])

fig.savefig('Fields.pdf',dpi=150)


plt.show()
