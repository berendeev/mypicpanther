
import numpy as np
import multiprocessing
#from multiprocessing import Process, Value, Array, Manager
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.colors as col
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import gridspec
from matplotlib import rc
import struct
from numpy.fft import rfft, rfftfreq
from math import sin, pi,sqrt
from numpy import array, arange,amax, argmax,sign, abs as np_abs
#f = open('rotJz', 'rb')
import copy
from ReadDataLib import *
from math import exp
from matplotlib.colors import LogNorm

cdict = {'red':	    ((0.0, 0.0, 0),
					 (0.35, 0.5859375, 0.5859375),
					 (0.4, 0, 0),#blue
					 (0.45, 0, 0),#0096ff
					 (0.495, 1, 1),#white
					 (0.505, 1, 1),#white
					 (0.55, 1, 1),#ff9600
					 (0.65, 1, 1),
                     (1.0, 0.5859375, 0.5859375)),
         'green':   ((0.0, 0.0, 0.0),
         		     (0.35, 0, 0),
         		     (0.4, 0, 0),#blue
         		     (0.45, 0.5859375, 0.5859375),#0096ff
         		     (0.495, 1, 1),#white
					 (0.505, 1, 1),#white
         		     (0.55, 0.5859375, 0.5859375),#ff9600
         		     (0.65, 0, 0),
                     (1.0, 0, 0)),
         'blue':    ((0.0, 0.5859375, 0.5859375),
                     (0.35, 1, 1),
                     (0.4, 1, 1),#blue
                     (0.45, 1, 1),#0096ff
                     (0.495, 1, 1),#white
					 (0.505, 1, 1),#white
                     (0.55, 0, 0),#ff9600
                     (0.65, 0.5859375, 0.5859375),
                     (1.0, 0, 0))}

phasecdict = {'red':(
(0.0, 1, 1),#white
(0.15, 0, 0),#0096ff
(0.35, 0, 0),#blue
(0.55, 1, 1),#ff9600
(0.75, 1, 1),
(1.0, 0.5859375, 0.5859375)),

'green':   (
(0.0, 1, 1),#white
(0.15, 0.5859375, 0.5859375),#0096ff
(0.35, 0, 0),#blue
(0.55, 0.5859375, 0.5859375),#ff9600
(0.75, 0, 0),
(1.0, 0, 0)),

'blue':    (
(0.0, 1, 1),#white
(0.15, 1, 1),#0096ff
(0.35, 1, 1),#blue
(0.55, 0, 0),#ff9600
(0.75, 0.5859375, 0.5859375),
(1.0, 0, 0))}

	
def Swap(data,Type):
	return np.swapaxes(data[Type],0,1)

	#неправильно работает, изменяет data
def Slice(data,Left,Right):
	data2=copy.deepcopy(data)
	data2['data']=data['data'][int(Left):int(Right)]
	return data2

def FastInt(data,dx,Nx):
	Int=0
	for x in range(0,Nx):
		Int+=data[x]
	Int*=dx
	return Int
	
def MakeE0(Data2D,SystemParameters,k):
	FileNameSignNum=len(str(int(int(SystemParameters['Max_Time'][0])/int(SystemParameters['Diagn_Time'][0]))))
	TimeStep='Last'
	if TimeStep=='Last':
		files=os.listdir('../../Fields/DiagXY/')#получили список файлов в каталоге с полями (они есть всегда)
		#print(files)
		files=sorted(files)
		TimeStep=files[-1][-FileNameSignNum:]
		TimeStepLast=TimeStep 
		
	print('TimeStepLast', TimeStepLast)
	
	Nx=int(SystemParameters['Nx'])
	Ny=int(SystemParameters['Ny'])
	PlasmaCy=int(SystemParameters['Y_PlasmaCells'][0])
	VacBottomCy=int(SystemParameters['Y_VacCellsBottom'][0])
	VacTopCy=int(SystemParameters['Y_VacCellsTop'][0])
	dx=float(SystemParameters['Cell_Dx'][0])
	dy=float(SystemParameters['Cell_Dy'][0])
	s01=float(SystemParameters['Lasers']['LEFT']['FocusWidth'][0])
	window=int(2*(pi/k)/dx)
	DataE_aver_y1=[]
	DataE_aver_y2=[]
	
	for y in range(0,PlasmaCy):
		#нащли E0(x,y)
		Int=np.sqrt(k/pi*np.convolve(Data2D['Ex']['data'][:][VacBottomCy+y]**2, np.ones((window,))*dx, mode='same')) #усреднение по окну
		
		#if y == PlasmaCy/2:
		#	axFieldEx.plot(Int,label='E0')
		#Возвести в четвёртую стпень
		#Int=Int
		#if y == PlasmaCy/2:
		#	axFieldEx.plot(Int,label='E0**4')
		DataE_aver_y1.append(Int)
		DataE_aver_y2.append(Int)
		
		#DataE_aver_y3.append(Int)
	#Усреднить по игрику
	masY=np.linspace(0,PlasmaCy*dy, PlasmaCy)
	Exp=np.zeros(PlasmaCy)
	for i in range(0, PlasmaCy):
		Exp[i]=exp(-(masY[i]-PlasmaCy*0.5*dy)*(masY[i]-PlasmaCy*0.5*dy)/(s01*s01))
	DataE_aver_y1=np.mean(DataE_aver_y1,axis=0)*PlasmaCy*dy #остаётся зависимость от продольной координаты
	print(Exp)
	for y in range(0,PlasmaCy):
		DataE_aver_y2[y]=np.asarray(DataE_aver_y2[y])*Exp[y]
		
	DataE_aver_y2=np.mean(DataE_aver_y2,axis=0)*PlasmaCy*dy
	FinInt=FastInt(DataE_aver_y1,dx,Nx)
	return DataE_aver_y1, DataE_aver_y2, FinInt


#сохраняет одномерный сигнал
#Type='Time'/'Coord'/'TimeFFT'/CoordFFT'

#ret[Type]=Type
#ret[Coord]=Coord
#ret[data]=FieldData[Type][Coord]
def SaveSignal(data,SysPar):
	#data=Swap(data,Type)
	if data['Type']=='InTime':#сигнал в момент времени от координаты
		dx=float(SysPar['Cell_Dx'][0])#шаг по времени
		x=np.arange(len(data['data']))*dx
		np.savetxt('Signal'+data['Sort']+'In'+str(data['Coord'])+'TimeStep.txt', np.c_[x,data['data']],header='TimeStep=: '+str(data['Coord'])+'\n'+'x Amp')

	if data['Type']=='InPoint':#сигнал в точке от времени
		dt=float(SysPar['dt'][0])#шаг по координате
		t=np.arange(len(data['data']))*dt
		np.savetxt('Signal'+data['Sort']+'In'+str(data['Coord'])+'Cell.txt', np.c_[t,data['data']],header='Cell=: '+str(data['Coord'])+'\n'+'t Amp')

	if data['Type']=='FFTInTime':#фурье по пространству
		print('ds')
		np.savetxt('FFTSignal'+data['Sort']+'In'+str(data['Coord'])+'TimeStep.txt', np.c_[data['fftx'],np_abs(data['spectrum'])/data['N']],header='TimeStep=: '+str(data['Coord'])+'\n'+'q Amp')


	if data['Type']=='FFTInPoint':#фурье по времени
		np.savetxt('FFTSignal'+data['Sort']+'In'+str(data['Coord'])+'Cell.txt', np.c_[data['fftx'],np_abs(data['spectrum'])/data['N']],header='Coord=: '+str(data['Coord'])+'\n'+'w Amp')

def Get1Ddens(Data,SysPar,Averaging):
	Data1D=copy.deepcopy(Data)

	print(Data1D)

	#dens[x][y]

	if Averaging==True:
			tempData=Data['data'][:][int(SysPar['Y_VacCellsBottom'][0]):int(SysPar['Y_VacCellsBottom'][0])+int(SysPar['Y_PlasmaCells'][0])]
			Data1D['data']=np.mean(tempData,axis=0)
	if Averaging==False:
			#Data1D['data']=Swap(Data,'data')
			#print(Data1D)
			Data1D['data']=Data1D['data'][:][int(int(SysPar['Ny'])/2)]
			print(Data1D)
	return Data1D
	
def Get1DFields(Data,SysPar,Coord):
	Data1D=copy.deepcopy(Data)

	print(Data1D)

	#dens[x][y]
	for field in Data1D.keys():
			Data1D[field]['data']=Data1D[field]['data'][:][int(Coord)]
			#print(Data1D)
	return Data1D

def Plot1D(Data,step):
	fig = plt.figure(figsize=(8, 4))
	gs =gridspec.GridSpec(1,1)#первый аргумент - вертикальное количество, второй - горизонтальное 
	ax= fig.add_subplot(gs[0])
	x=np.arange(len(Data['data']))*step
	ax.plot(x,Data['data'], label=Data['Type']+' '+Data['Sort']+' '+str(Data['Coord']))
	ax.legend()
	plt.show()

def moving_average(a, n):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n
    
def PlotBeamFieldEfficiency(IntegEnergy,SystemParameters,ax,fig):
	ax.plot(Data1D, color="C3")

	return 0

def Plot1Ddens(DensData,Sort,Direction,Coord,Vmin,Vmax,title,SystemParameters,ax,fig):
	#Direction - 'long' или 'trans'
	#Coord - координата на которой делать срез Игрик для long и Икс для trans
	if(Direction=='long'):
		tempData=DensData[Sort]['data'][:][int(SystemParameters['Y_VacCellsBottom'][0]):int(SystemParameters['Y_VacCellsBottom'][0])+int(SystemParameters['Y_PlasmaCells'][0])]
		print(tempData)
		Data1D=np.mean(tempData,axis=0)
		ax.set_xlim(int(SystemParameters['X_DampCellsLeft'][0]), int(SystemParameters['Nx'])-int(SystemParameters['X_DampCellsRight'][0]))

	if(Direction=='trans'):#похоже что не работает
		print('!!',Direction)
		Data1D=DensData[Sort]['data'][Coord][:]
		ax.set_xlim(int(SystemParameters['Y_DampCellsBottom'][0]), int(SystemParameters['Ny'])-int(SystemParameters['Y_DampCellsTop'][0]))

	ax.plot(Data1D, color="C3")
	ax.set_ylim(Vmin,Vmax)
	ax.tick_params(axis='y', colors="C3")
	ax.set_xlabel('$x$')
	ax.set_ylabel('dens', color="C3")
	ax.yaxis.grid() # horizontal lines

	return 0

def Plot1DField(FieldData,Type,Coord,Vmin,Vmax,title,SystemParameters,ax,fig):
	#Direction - 'long' или 'trans'
	#Coord - координата на которой делать срез Игрик для long и Икс для trans
	Data1D=FieldData[Type]['data'][:][Coord]
	ax.set_xlim(int(SystemParameters['X_DampCellsLeft'][0]), int(SystemParameters['Nx'])-int(SystemParameters['X_DampCellsRight'][0]))
	ax.plot(Data1D, color="C0",lw=0.4,label=Type)
	ax.set_ylim(Vmin,Vmax)
	ax.yaxis.grid() # horizontal lines

	#ax.set_ylabel(Type,color="C0")
	ax.yaxis.tick_right()
	ax.tick_params(
			axis='x',          # changes apply to the x-axis
			which='both',      # both major and minor ticks are affected
			bottom='off',      # ticks along the bottom edge are off
			top='off',         # ticks along the top edge are off
			labelbottom='off') # labels along the bottom edge are off
	ax.yaxis.set_label_position('right')
	ax.tick_params(axis='y', colors="C0")
	ax.legend()
	return 0
	
def Get1DInPoint(FieldData,Sort,Coord,SysPar,PltTr):
	if PltTr==True:
		fig = plt.figure(figsize=(8, 4))
		gs =gridspec.GridSpec(1,1)#первый аргумент - вертикальное количество, второй - горизонтальное 
		ax= fig.add_subplot(gs[0])
		dt=float(SysPar['dt'][0])#шаг по  времени
		data=FieldData[Sort][Coord]
		t=np.arange(len(data))*dt
	
		ax.plot(t,data,label=Sort+' in '+str(Coord)+' cell')

		ax.set_xlabel('$t/\omega_p$')
		ax.set_ylabel(Sort)
		ax.legend()
		plt.show()
	ret={}
	ret['Type']='InPoint'
	ret['Sort']=Sort
	ret['Coord']=Coord
	ret['data']=FieldData[Sort][Coord]
	return  ret
	
def Get1DInTime(FieldData,Sort,Coord,SysPar,PltTr):
	data=np.swapaxes(FieldData[Sort],0,1)[Coord]
	if PltTr==True:
		fig = plt.figure(figsize=(8, 4))
		gs =gridspec.GridSpec(1,1)#первый аргумент - вертикальное количество, второй - горизонтальное 
		ax= fig.add_subplot(gs[0])
		dx=float(SysPar['Cell_Dx'][0])#шаг по координате
		x=np.arange(len(data))*dx

		ax.plot(x,data,label=Sort+' in '+str(Coord)+' timeStep')
		ax.set_xlabel('$x$, $c/\omega_p$')
		ax.set_ylabel(Sort)
		ax.legend()
		plt.show()
	ret={}
	ret['Type']='InTime'
	ret['Sort']=Sort
	ret['Coord']=Coord
	ret['data']=data
	return  ret

def Plot2Ddens(DensData,Sort,Vmin,Vmax,title,SystemParameters,ax,fig):
	im=ax.imshow(DensData[Sort]['data'], vmin=Vmin, vmax=Vmax, cmap='Greys',origin='lower',aspect='auto')
	ax.set_xlabel('$x$')
	ax.set_ylabel('$y$')
	ax.set_title(title)
	divider = make_axes_locatable(ax)
	cax = divider.append_axes("bottom", size="2%", pad=0.6)#свойства colorbar'а
	cbar =fig.colorbar(im, cax=cax, orientation='horizontal')
	ax.set_xlim(int(SystemParameters['X_DampCellsLeft'][0]), int(SystemParameters['Nx'])-int(SystemParameters['X_DampCellsRight'][0]))
	ax.set_ylim(int(SystemParameters['Y_DampCellsBottom'][0]), int(SystemParameters['Ny'])-int(SystemParameters['Y_DampCellsTop'][0]))
	#tick_locator = ticker.MaxNLocator(nbins=3)
	#cbar.locator = tick_locator
	cbar.formatter.set_powerlimits((0, 0))
	cbar.ax.xaxis.set_ticks_position('bottom')
	cbar.update_ticks()
	
def Plot2DphasePtPl(DensData,Sort,Vmin,Vmax,title,SystemParameters,ax,fig):
	pl_min=float(SystemParameters['Particles'][Sort]['PhaseMinX_Y'][0])
	pl_max=float(SystemParameters['Particles'][Sort]['PhaseMaxX_Y'][0])
	pt_min=float(SystemParameters['Particles'][Sort]['PhaseMinX_Y'][1])
	pt_max=float(SystemParameters['Particles'][Sort]['PhaseMaxX_Y'][1])
	cm = col.LinearSegmentedColormap('phase',phasecdict,N=1024,gamma=1)

	#im=ax.imshow(DensData[Sort]['data'], vmin=Vmin, vmax=Vmax, cmap=cm,origin='lower',aspect='auto',extent=[pl_min,pl_max,pt_min,pt_max])
	im=ax.imshow(DensData[Sort]['data'],  cmap=cm,origin='lower',aspect='equal',norm=LogNorm(vmin=0.01, vmax=10000),extent=[pl_min,pl_max,pt_min,pt_max])
	ax.set_xlabel('$p_\parallel$')
	ax.set_ylabel('$p_\perp$')
	ax.set_title(title)
	divider = make_axes_locatable(ax)
	cax = divider.append_axes("bottom", size="2%", pad=0.6)#свойства colorbar'а
	cbar =fig.colorbar(im, cax=cax, orientation='horizontal')
	#tick_locator = ticker.MaxNLocator(nbins=3)
	#cbar.locator = tick_locator
	#$cbar.formatter.set_powerlimits((0, 0))
	cbar.ax.xaxis.set_ticks_position('bottom')
	cbar.update_ticks()
	
def Plot2Dphase(PhaseData,Sort,MomType,Vmin,Vmax,title,SystemParameters,ax,fig):
	cm = col.LinearSegmentedColormap('phase',phasecdict,N=1024,gamma=1)
	im=ax.imshow(PhaseData[Sort][MomType], vmin=2*int(SystemParameters['PartNumPerCell'][0]), vmax=Vmax, cmap=cm,origin='lower',aspect='auto')
	ax.set_xlabel('$x$')
	y=[0,int(SystemParameters['PhaseDiagNum'][0])]
	if(MomType=='vx'):
		ax.set_ylabel('$v_x$')
		labels = [SystemParameters['Particles'][Sort]['PhaseMinX_Y'][0], SystemParameters['Particles'][Sort]['PhaseMaxX_Y'][0]]
		if(float(SystemParameters['Particles'][Sort]['PhaseMinX_Y'][0])<0):
			dv=(float(SystemParameters['Particles'][Sort]['PhaseMaxX_Y'][0])-float(SystemParameters['Particles'][Sort]['PhaseMinX_Y'][0]))/float(SystemParameters['PhaseDiagNum'][0])
			labels.append('0')
			y.append(-float(SystemParameters['Particles'][Sort]['PhaseMinX_Y'][0])/dv)
		if(Sort[-4:]=='BEAM'):
			vb=float(SystemParameters['Particles'][Sort]['Velocity'][0])
			dv=(float(SystemParameters['Particles'][Sort]['PhaseMaxX_Y'][0])-float(SystemParameters['Particles'][Sort]['PhaseMinX_Y'][0]))/float(SystemParameters['PhaseDiagNum'][0])
			vby=vb/dv-float(SystemParameters['Particles'][Sort]['PhaseMinX_Y'][0])/dv
			#print('!!!',vb,dv,vby)
			labels.append(str(round(vb,2)))
			y.append(vby)
			#x1, y1 = [0, int((SystemParameters['PhaseDiagNum'][0]))], [vby, vby]
			#plt.plot(x1, y1, '--')
	if(MomType=='vy'):
		ax.set_ylabel('$v_y$')
		labels = [SystemParameters['Particles'][Sort]['PhaseMinX_Y'][1], SystemParameters['Particles'][Sort]['PhaseMaxX_Y'][1]]

		if(float(SystemParameters['Particles'][Sort]['PhaseMinX_Y'][1])<0):
			dv=(float(SystemParameters['Particles'][Sort]['PhaseMaxX_Y'][1])-float(SystemParameters['Particles'][Sort]['PhaseMinX_Y'][1]))/float(SystemParameters['PhaseDiagNum'][0])
			labels.append('0')
			y.append(-float(SystemParameters['Particles'][Sort]['PhaseMinX_Y'][1])/dv)
	plt.yticks(y, labels)
	ax.yaxis.grid() # horizontal lines
	coordConvert=float(SystemParameters['PhaseDiagCoordNum'][0])/SystemParameters['Nx']
	ax.set_xlim(float(SystemParameters['X_DampCellsLeft'][0])*coordConvert, float(SystemParameters['PhaseDiagCoordNum'][0])-float(SystemParameters['X_DampCellsRight'][0])*coordConvert)
	divider = make_axes_locatable(ax)
	cax = divider.append_axes("bottom", size="2%", pad=0.6)#свойства colorbar'а
	cbar =fig.colorbar(im, cax=cax, orientation='horizontal')
	#ax.set_xlim(int(SystemParameters['X_DampCellsLeft'][0]), int(SystemParameters['Nx'])-int(SystemParameters['X_DampCellsRight'][0]))
	#ax.set_ylim(int(SystemParameters['Y_DampCellsBottom'][0]), int(SystemParameters['Ny'])-int(SystemParameters['Y_DampCellsTop'][0]))
	#tick_locator = ticker.MaxNLocator(nbins=3)
	#cbar.locator = tick_locator
	cbar.formatter.set_powerlimits((0, 0))
	cbar.ax.xaxis.set_ticks_position('bottom')
	cbar.update_ticks()
	
def Plot2Dfields(FieldData,Type,Vmin,Vmax,title,SystemParameters,ax,fig):
	#x = np.arange(SystemParameters['Nx'])*float(SystemParameters['Cell_Dx'][0])
	#y = np.arange(SystemParameters['Ny'])*float(SystemParameters['Cell_Dx'][0])
	#X, Y = np.meshgrid(x,y)
	#print(X,Y)
	cm = col.LinearSegmentedColormap('fields',cdict,N=1024,gamma=1)
	im=ax.imshow(FieldData[Type]['data'], vmin=Vmin, vmax=Vmax, cmap=cm,origin='lower',aspect='auto')
	#ax.xaxis.set_ticks(x)
	#ax.yaxis.set_ticks(y)
	#locator=ticker.MaxNLocator(prune='both', nbins=5)
	#ax.yaxis.set_major_locator(locator)
	#ax.xaxis.set_major_locator(locator)
	#ax.set_title(title)
	ax.set_xlabel('$x$')
	ax.set_ylabel('$y$')
	ax.set_title(title)
	#ax.set_xlim(int(SystemParameters['X_DampCellsLeft'][0]), int(SystemParameters['Nx'])-int(SystemParameters['X_DampCellsRight'][0]))
	#ax.set_ylim(int(SystemParameters['Y_DampCellsBottom'][0]), int(SystemParameters['Ny'])-int(SystemParameters['Y_DampCellsTop'][0]))
	divider = make_axes_locatable(ax)
	cax = divider.append_axes("bottom", size="2%", pad=0.6)#свойства colorbar'а
	cbar =fig.colorbar(im, cax=cax, orientation='horizontal')
	tick_locator = ticker.MaxNLocator(nbins=3)
	cbar.locator = tick_locator
		#cb = fig.colorbar(im,cax=cax, orientation='horizontal')#,ticks=[-1e-6,-0.5e-6,0,0.5e-6,1e-6]
	#cbar = fig.colorbar(cax, ticks=[-1, 0, 1], orientation='horizontal')
	#cbar.ax.set_xticklabels(['Low', 'Medium', 'High'])  # horizontal colorbar
	#cb = ax.colorbar(im,cax=cax, orientation='horizontal')#,ticks=[-1e-6,-0.5e-6,0,0.5e-6,1e-6]
	#cb.outline.set_linewidth(0.2)#ширина border'а у colorbar'а
	cbar.formatter.set_powerlimits((0, 0))
	#cb.format=ticker.FuncFormatter(fmt)	
	cbar.ax.xaxis.set_ticks_position('bottom')
	cbar.update_ticks()


def PlotIntegMPI(IntegEnergy,SystemParameters,ax):
	time=np.arange(0,len(IntegEnergy['AllFields']))*float(SystemParameters['dt'][0])
	ax.plot(time,IntegEnergy['AllFields']+IntegEnergy['0']['IONS_Chg']+IntegEnergy['1']['ELECTRONS_Chg']+IntegEnergy['2']['LEFTBEAM_Chg'],label=SystemParameters['FolderName'][0])
	ax.set_xlabel('$t/\omega_p$')


def FindHHWidth(spectrum,N,res):
	maximum=amax( np_abs(spectrum))/N
	tr=0
	for i in range(len(spectrum)):
		temp=np_abs(spectrum[i])/N
		#print(i,temp)
		if (temp>=maximum*0.5)&(tr==0):
			res[0]=i
			print("res[0]",i,temp,maximum*0.5)
			tr=1
		if (temp<=maximum*0.5)&(tr==1):
			res[1]=i
			print("res[1]",i,temp)

			tr=2
		if tr==2:
			break
			
def fwhm(x, y, k=10):
    """
    Determine full-with-half-maximum of a peaked set of points, x and y.

    Assumes that there is only one peak present in the datasset.  The function
    uses a spline interpolation of order k.
    """

    class MultiplePeaks(Exception): pass
    class NoPeaksFound(Exception): pass

    half_max = amax(y)/2.0
    s = splrep(x, y - half_max)
    #print(s)

    roots = sproot(s)
   # if len(roots) > 2:
    #    raise MultiplePeaks("The dataset appears to have multiple peaks, and "
              #  "thus the FWHM can't be determined.")
   # elif len(roots) < 2:
    #    raise NoPeaksFound("No proper peaks were found in the data set; likely "
              #  "the dataset is flat (e.g. all zeros).")
    #else:
    return abs(roots[1] - roots[0])

#Фурье
def FFT(Signal,step):
	N=len(Signal)
	FD = 1./step # частота дискретизации, отсчётов в секунду
	spectrum = rfft(Signal)
	fftx=rfftfreq(N, 1./FD)*2*pi
	Rad_freq=fftx[argmax( np_abs(spectrum))]
	print("Max_freq=",Rad_freq)
	Spectr={}
	Spectr['spectrum']=spectrum

	Spectr['fftx']=fftx
	Spectr['Rad_freq']=Rad_freq
	Spectr['N']=N
	#print(Spectr)
	return Spectr

def PlotFFT(Data,SysPar):
#PlotFFT(dataC,'Coord',Coord,SysPar)#спектр по времени (w)
	Max=4#предел по иксу
	fig = plt.figure(figsize=(8, 4))
	gs =gridspec.GridSpec(1,1)#первый аргумент - вертикальное количество, второй - горизонтальное 
	ax= fig.add_subplot(gs[0])
	if Data['Type']=='InPoint':#сигнал в точке пространства
		Step=float(SysPar['dt'][0])#шаг по времени
		ax.set_xlabel('$\omega/\omega_p$')
	if Data['Type']=='InTime':#сигнал в точке времени
		Step=float(SysPar['Cell_Dx'][0])#шаг по координате
		ax.set_xlabel('$q$')
	Spectr=FFT(Data['data'],Step)
	ax.set_xlim(0, Max)
#axFFT.set_ylim(bottom=0,top=5e-3)

	tick_locator = ticker.MaxNLocator(nbins=5)
	formatter = ticker.FormatStrFormatter ("%.2f")
	formatter = ticker.ScalarFormatter ()
	formatter.set_powerlimits((-1, 2))
	# Установка форматера для оси Y
	ax.yaxis.set_major_formatter (formatter)
	ax.yaxis.set_major_locator(tick_locator)
	ax.set_ylabel(r'$Amp$')
	#ax.plot([Spectr['Rad_freq'],Spectr['Rad_freq']], [0,2.5e-5],color='red', lw=1)
	ax.plot(Spectr['fftx'], np_abs(Spectr['spectrum'])/Spectr['N'],color='green')
	plt.show()
	Spectr['Coord']=Data['Coord']
	Spectr['Type']='FFT'+Data['Type']
	Spectr['Sort']=Data['Sort']
	return Spectr




	
def MakeFFT3D(Fields1D,SystemParameters,Start,Finish,title,field,ProcNum):
	def yfmt(x, pos):
		a=x*MaxFreq
		return '$'+str('{:.2f}'.format(a))+'$'
	Nx=int(SystemParameters['Nx'])
	Ny=int(SystemParameters['Ny'])
	dt=float(SystemParameters['dt'][0])
	Spectr=[None] * (Finish-Start)
	Spectr3Dt=[None] * (Finish-Start)
	Spectr3D={}
	print(0,Finish,Start)
	maxFFtxel=0
	
	#print(Spectr3Dt)
	manager = multiprocessing.Manager()
	Spectr = manager.list(range(Finish-Start))
	Spectr3Dt = manager.list(range(Finish-Start))
    
	def Ffunc(rank,ui):
		i=rank
		for x in range(Start+rank,Finish,ProcNum):
			print('rank=',rank,'x=',x)
			tempSpectr=FFT(Fields1D[field][x],dt)
			#print(tempSpectr)

			Spectr[i]=(tempSpectr)
			Spectr3Dt[i]=(tempSpectr['spectrum'])
			#Spectr.append(tempSpectr)
			#Spectr3Dt.append(tempSpectr['spectrum'][:maxFFtxel])
			i+=ProcNum

	jobs = []
	# Setup a list of processes that we want to run
	processes = [multiprocessing.Process(target=Ffunc, args=(x, 0)) for x in range(ProcNum)]

	#for proc in range(ProcNum):
	#	p = multiprocessing.Process(target=Ffunc, args=(proc,0))
	#	jobs.append(p)
	#	p.start()
	#queue.join_thread()
	#for proc in range(ProcNum):
	#	jobs[proc].join(20)
	#	print('proc',proc)
# Run processes
	for p in processes:
		p.start()

	# Exit the completed processes
	for p in processes:
		p.join()

	#print('Spectr3Dt',Spectr3Dt)
	Spectr3Dt=np.asarray(Spectr3Dt)
	Spectr3D['data']=np.swapaxes(Spectr3Dt,0,1)
	Spectr3D['N']=Spectr[0]['N']
	Spectr3D['fftx']=Spectr[0]['fftx']
	Spectr3D['Start']=Start
	Spectr3D['Finish']=Finish
	#Spectr3D['MaxFreq']=MaxFreq
	#pprint.pprint(Spectr3D)
	#maxFFtxel=np.argmax(Spectr3D['fftx']>4)
	#print("element=", maxFFtxel,Spectr3D['fftx'][maxFFtxel] )
	np.save('./Spectr'+title, Spectr3D)
	return Spectr3D
	
	
def PlotFFT3D(Spectr3D,axFFT3D,SystemParameters,MaxFreq,Start,Finish,title,field,omega,Vmax):
	def yfmt(x, pos):
		a=x*MaxFreq
		return '$'+str('{:.2f}'.format(a))+'$'
	Nx=int(SystemParameters['Nx'])
	Ny=int(SystemParameters['Ny'])
	dt=float(SystemParameters['dt'][0])
	cm = col.LinearSegmentedColormap('phase',phasecdict,N=1024,gamma=1)
	
	np.save('./Spectr'+title, Spectr3D)
	axFFT3D.set_ylim(0, MaxFreq)
	axFFT3D.set_xlim(Start, Finish)
	axFFT3D.set_title(title,fontsize=15)

	x=np.linspace(Start,Finish,Finish-Start)
	y=Spectr3D['fftx']
	X,Y = np.meshgrid(x, y)
	#print(Spectr3D['fftx'])
	pcm = axFFT3D.pcolormesh(X,Y,np_abs(Spectr3D['data'])/Spectr3D['N'], vmin=0, vmax=Vmax, cmap=cm, rasterized=True) # you don't need rasterized=True
	axFFT3D_2 = axFFT3D.twinx()
	axFFT3D_2.yaxis.set_tick_params(which='major', direction='out')
	Omega=float(SystemParameters['UniformB'][0])
	wuh=sqrt(omega*omega+Omega*Omega)
	freq=[omega/MaxFreq,2*omega/MaxFreq,3*omega/MaxFreq,Omega/MaxFreq,2*Omega/MaxFreq,wuh/MaxFreq]
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, omega/MaxFreq,  r'$\omega_p$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, 2*omega/MaxFreq,  r'$2\omega_p$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, 3*omega/MaxFreq,  r'$3\omega_p$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, wuh/MaxFreq,  r'$\omega_{uh}$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, Omega/MaxFreq,  r'$\Omega$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, 2*Omega/MaxFreq,  r'$2\Omega$',color='red',fontsize=15,ha='left',va='center')

	#convcwp=1./dx/Ny
	#yticksExcwp=[-20*convcwp,-10*convcwp,-4.2*convcwp,0*convcwp,4.2*convcwp,10*convcwp,20*convcwp]
	axFFT3D_2.yaxis.set_major_formatter(ticker.FuncFormatter(yfmt))
	axFFT3D_2.set_yticks(freq)
	axFFT3D_2.tick_params(axis='x', colors='blue')
	
def PlotFFT3DwithText(Fields1D,axFFT3D,SystemParameters,MaxFreq,Start,Finish,title,field,omega):
	directory='MidFFT_'+title
	if not os.path.exists(directory):
		os.makedirs(directory)
		
	def yfmt(x, pos):
		a=x*MaxFreq
		return '$'+str('{:.2f}'.format(a))+'$'
	Nx=int(SystemParameters['Nx'])
	Ny=int(SystemParameters['Ny'])
	dt=float(SystemParameters['dt'][0])
	cm = col.LinearSegmentedColormap('phase',phasecdict,N=1024,gamma=1)
	Spectr=[]
	Spectr3Dt=[]
	Spectr3D={}
	print(0,Finish,Start)
	maxFFtxel=0
	for x in range(Start,Finish):
		print(x)
		tempSpectr={}
		tempSpectr['spectrum'],tempSpectr['fftx'],tempSpectr['Rad_freq'],tempSpectr['N']=FFT(Fields1D[field][x],dt)
		maxFFtxel=np.argmax(tempSpectr['fftx']>MaxFreq)
		Spectr.append(tempSpectr)
		Spectr3Dt.append(tempSpectr['spectrum'][:maxFFtxel])
		np.savetxt('./'+directory+'/FFT_'+field+"_x="+str(x), np.c_[tempSpectr['fftx'][:maxFFtxel],np_abs(tempSpectr['spectrum'][:maxFFtxel])/tempSpectr['N']]) 
	Spectr3D['data']=np.swapaxes(Spectr3Dt,0,1)
	Spectr3D['N']=Spectr[0]['N']
	Spectr3D['fftx']=Spectr[0]['fftx'][:maxFFtxel]
	Spectr3D['Start']=Start
	Spectr3D['Finish']=Finish
	Spectr3D['MaxFreq']=MaxFreq
	#pprint.pprint(Spectr3D)
	#maxFFtxel=np.argmax(Spectr3D['fftx']>4)
	#print("element=", maxFFtxel,Spectr3D['fftx'][maxFFtxel] )
	np.save('./Spectr'+title, Spectr3D)
	axFFT3D.set_ylim(0, MaxFreq)
	axFFT3D.set_xlim(Start, Finish)
	axFFT3D.set_title(title,fontsize=15)

	x=np.linspace(Start,Finish,Finish-Start)
	y=Spectr3D['fftx']
	X,Y = np.meshgrid(x, y)
	pcm = axFFT3D.pcolormesh(X,Y,np_abs(Spectr3D['data'])/Spectr3D['N'], vmin=0,  cmap=cm, rasterized=True) # you don't need rasterized=True
	axFFT3D_2 = axFFT3D.twinx()
	axFFT3D_2.yaxis.set_tick_params(which='major', direction='out')
	Omega=float(SystemParameters['UniformB'][0])
	wuh=sqrt(omega*omega+Omega*Omega)
	freq=[omega/MaxFreq,2*omega/MaxFreq,3*omega/MaxFreq,Omega/MaxFreq,2*Omega/MaxFreq,wuh/MaxFreq]
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, omega/MaxFreq,  r'$\omega_b$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, 2*omega/MaxFreq,  r'$2\omega_b$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, 3*omega/MaxFreq,  r'$3\omega_b$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, wuh/MaxFreq,  r'$\omega_{uh}$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, Omega/MaxFreq,  r'$\Omega$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, 2*Omega/MaxFreq,  r'$2\Omega$',color='red',fontsize=15,ha='left',va='center')

	#convcwp=1./dx/Ny
	#yticksExcwp=[-20*convcwp,-10*convcwp,-4.2*convcwp,0*convcwp,4.2*convcwp,10*convcwp,20*convcwp]
	axFFT3D_2.yaxis.set_major_formatter(ticker.FuncFormatter(yfmt))
	axFFT3D_2.set_yticks(freq)
	axFFT3D_2.tick_params(axis='x', colors='blue')
	
def PlotFFT3DwithText_k(Fields1D,axFFT3D,SystemParameters,MaxK,Start,Finish,Step,title,field,omega):
	directory='MidFFT_'+title
	if not os.path.exists(directory):
		os.makedirs(directory)
		
	def yfmt(x, pos):
		a=x*MaxK
		return '$'+str('{:.2f}'.format(a))+'$'
	Nx=int(SystemParameters['Nx'])
	Ny=int(SystemParameters['Ny'])
	dx=float(SystemParameters['Cell_Dx'][0])
	dt=float(SystemParameters['dt'][0])

	cm = col.LinearSegmentedColormap('phase',phasecdict,N=1024,gamma=1)
	Spectr=[]
	Spectr3Dt=[]
	Spectr3D={}
	Fields1D[field]=np.swapaxes(Fields1D[field],0,1)
	print(0,Finish,Start)
	maxFFtxel=0
#	for x in range(Start,Finish):
	for t in range(0,Finish,Step):
		print(t)
		tempSpectr={}
		tempSpectr['spectrum'],tempSpectr['fftx'],tempSpectr['Rad_freq'],tempSpectr['N']=FFT(Fields1D[field][t],dx)
		maxFFtxel=np.argmax(tempSpectr['fftx']>MaxK)
		Spectr.append(tempSpectr)
		Spectr3Dt.append(tempSpectr['spectrum'][:maxFFtxel])
		np.savetxt('./'+directory+'/FFT_'+field+"_t="+str(t), np.c_[tempSpectr['fftx'][:maxFFtxel],np_abs(tempSpectr['spectrum'][:maxFFtxel])/tempSpectr['N']]) 
	Spectr3D['data']=np.swapaxes(Spectr3Dt,0,1)
	Spectr3D['N']=Spectr[0]['N']
	Spectr3D['fftx']=Spectr[0]['fftx'][:maxFFtxel]
	Spectr3D['Start']=Start
	Spectr3D['Finish']=Finish
	Spectr3D['MaxFreq']=MaxK
	#pprint.pprint(Spectr3D)
	#maxFFtxel=np.argmax(Spectr3D['fftx']>4)
	#print("element=", maxFFtxel,Spectr3D['fftx'][maxFFtxel] )
	np.save('./SpectrK'+title, Spectr3D)
#	axFFT3D.set_ylim(0, MaxK)
#	axFFT3D.set_xlim(Start, Finish/Step)
	#axFFT3D.set_title(title,fontsize=15)
	#axFFT3D.plot(Fields1D[field][7000])

	x=np.linspace(0,int((Finish-Start)/Step)+1,int((Finish-Start)/Step)+1)*Step*dt
	y=Spectr3D['fftx']
	X,Y = np.meshgrid(x, y)
	pcm = axFFT3D.pcolormesh(X,Y,np_abs(Spectr3D['data'])/Spectr3D['N'], vmin=0,  cmap=cm, rasterized=True) # you don't need rasterized=True
#	axFFT3D_2 = axFFT3D.twinx()
#	axFFT3D_2.yaxis.set_tick_params(which='major', direction='out')

	#convcwp=1./dx/Ny
	#yticksExcwp=[-20*convcwp,-10*convcwp,-4.2*convcwp,0*convcwp,4.2*convcwp,10*convcwp,20*convcwp]
	#axFFT3D_2.yaxis.set_major_formatter(ticker.FuncFormatter(yfmt))
	#axFFT3D_2.set_yticks(freq)
	##axFFT3D_2.tick_params(axis='x', colors='blue')
	
def PlotLoadFFT3D(axFFT3D,SystemParameters,title,fileName,Vmax,MaxFreq,omega):
	Nx=int(SystemParameters['Nx'])
	Ny=int(SystemParameters['Ny'])
	dt=float(SystemParameters['dt'][0])
	def yfmt(x, pos):
		a=x*MaxFreq
		return '$'+str('{:.2f}'.format(a))+'$'
	cm = col.LinearSegmentedColormap('phase',phasecdict,N=1024,gamma=1)
	Spectr3D=np.load(fileName)
	Start=Spectr3D.item().get('Start')
	Finish=Spectr3D.item().get('Finish')
	#MaxFreq=Spectr3D.item().get('MaxFreq')
	#pprint.pprint(Spectr3D)
	#pprint.pprint(Spectr3D.item().get('data'))
	#pprint.pprint(Spectr3D['fftx'][0])
	axFFT3D.set_ylim(0, MaxFreq)
	axFFT3D.set_xlim(Start, Finish)
	axFFT3D.set_title(title,fontsize=15)

	x=np.linspace(Start,Finish,Finish-Start)
	y=Spectr3D.item().get('fftx')
	X,Y = np.meshgrid(x, y)
	if Vmax!='auto':
		pcm = axFFT3D.pcolormesh(X,Y,np_abs(Spectr3D.item().get('data'))/Spectr3D.item().get('N'), vmin=0, vmax=Vmax, cmap=cm, rasterized=True) # you don't need rasterized=True
	else:
		pcm = axFFT3D.pcolormesh(X,Y,np_abs(Spectr3D.item().get('data'))/Spectr3D.item().get('N'), vmin=0, cmap=cm, rasterized=True) # you don't need rasterized=True
	axFFT3D_2 = axFFT3D.twinx()
	axFFT3D_2.yaxis.set_tick_params(which='major', direction='out')
	Omega=float(SystemParameters['UniformB'][0])
	wuh=sqrt(omega*omega+Omega*Omega)
	print('MaxFreq',MaxFreq)

	freq=[omega/MaxFreq,2*omega/MaxFreq,3*omega/MaxFreq,Omega/MaxFreq,2*Omega/MaxFreq,wuh/MaxFreq]
	print('freq',freq)

	axFFT3D_2.text(Finish+5*(Finish-Start)/50, omega/MaxFreq,  r'$\omega_b$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, 2*omega/MaxFreq,  r'$2\omega_b$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, 3*omega/MaxFreq,  r'$3\omega_b$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, wuh/MaxFreq,  r'$\omega_{uh}$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, Omega/MaxFreq,  r'$\Omega$',color='red',fontsize=15,ha='left',va='center')
	axFFT3D_2.text(Finish+5*(Finish-Start)/50, 2*Omega/MaxFreq,  r'$2\Omega$',color='red',fontsize=15,ha='left',va='center')

	#convcwp=1./dx/Ny
	#yticksExcwp=[-20*convcwp,-10*convcwp,-4.2*convcwp,0*convcwp,4.2*convcwp,10*convcwp,20*convcwp]
	axFFT3D_2.yaxis.set_major_formatter(ticker.FuncFormatter(yfmt))
	axFFT3D_2.set_yticks(freq)
	axFFT3D_2.tick_params(axis='x', colors='blue')
